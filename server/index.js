console.log("Implement servermu disini yak 😝!");

const express = require('express');
const path = require('path');

const app = express();
app.use(express.static('public'));


const port = 3000;

// sendFile will go here
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, '/index.html'));
});

app.get('/cars', function(req, res) {
    res.sendFile(path.join(__dirname, '../public/cars.html'));
});


app.listen(port);
console.log('Server started at http://localhost:' + port);