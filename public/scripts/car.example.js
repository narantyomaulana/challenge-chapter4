class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
      <div class="row justify-content-center mt-3">
          <div class="card col-lg-3 mx-2 " style="width: 22rem; height: 34rem;">
              <img src="${this.image}" style="height: 35%; width:100%; object-fit: cover" class="card-img mt-2" alt="..." >
              <div class="card-body">
                  <p>${this.available}/${this.model}</p>
                  <h5 class="price fw-bold">${this.rentPerDay} / hari</h5>
                  <p class="title">${this.description}</p>
                  <p><i class="fa fa-users" aria-hidden="true"></i>&ensp;${this.capacity} Orang</p>
                  <p><i class="fa fa-cog" aria-hidden="true"></i>&ensp;${this.transmission} </p>
                  <p><i class="fa fa-calendar" aria-hidden="true"></i>&ensp;${this.year} </p>
                  <button class="btn-cari">Pilih Mobil</button>
              </div>
          </div>
      </div>
    `;
  }
}
