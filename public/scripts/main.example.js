/*
 * Contoh kode untuk membaca query parameter,
 * Siapa tau relevan! :)
 * */

const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());

// Coba olah data ini hehe :)
console.log(params);

/*
 * Contoh penggunaan DOM di dalam class
 * */
const app = new App();

const submitBtn = document.getElementById("submitBtn");
submitBtn.addEventListener("click", () => {
  app.init().then(app.run);
});

submitBtn.addEventListener('mouseleave',()=>{
    inactiveDarkBackground();
})

